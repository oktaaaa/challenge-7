var express = require('express');
var router = express.Router();
let isLogin = true
/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index')
});

router.get('/play-game', (req, res) =>{
  if(isLogin){
      res.render('rsp_game')
      return
  }
  
})
module.exports = router;
