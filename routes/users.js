var express = require('express');
var router = express.Router();

const userActions = require('../controllers/userControllers')
const restrict = require('../middlewares/restrict')
/* GET users listing. */



//CREATE users
router.get('/signup', userActions.signupPage)
router.get('/signup', userActions.signUpUsers)


//get the users
router.get('/login', userActions.loginPageUsers)
router.post('/login', userActions.loginUsers)

//update
//display update
router.get('/users/update/:id', userActions.updatePageUsers)
router.put('/users/update/:id', userActions.updateUsers)

//Delete
router.get('/users/delete/:id', userActions.deleteUsers)

module.exports = router;