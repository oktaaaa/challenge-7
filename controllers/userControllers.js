
const {UserGame, UserGameBiodata} = require('../models');

const signupPage = (req, res, next) =>{
    res.render('create-user')
} 

const signUpUsers = async (req, res, next) =>{
  
    const {username, password} = req.body
    try{
        await UserGame.create({
            username, 
            password,
            userGameBiodata: {
              firstName: req.body.firstName,
              lastName: req.body.lastName,
              birthday: req.body.birthday,
              gender: req.body.gender,
              phone: req.body.phone,
              location: req.body.location
            }
          }, {
            include: [{
              model: UserGameBiodata,
              as: "UserGameBiodatum"
            }]
        })
        res.redirect('/login')
    }catch(err) {
        console.log(err)
        res.sendStatus(500)
    }

}

//login 
const loginPageUsers = (req, res, next) =>{
  res.render('login')
}

const loginUsers = (req, res, next) =>{
  UserGame.authenticate(req.body)
    .then(user => {
        res.json({
            id: user.id,
            username: user.username,
            accessToken: user.generateToken()
        })
    })
    .catch(err => {
        console.log(err);
        next(err)
    })

}

//for every user

//update
const updatePageUsers = (req, res, next) =>{
  res.render('update')
}

const updateUsers = (req, res) =>{
  const {password} = req.body
  UserGame.update({
    password
  }, {
    where: {id: req.params.id}
  }).then(user =>{
    res.redirect('/users')
  })
}

//delete
const deleteUsers = (req, res, next) =>{
  UserGame.destroy({
    where: {id: req.params.id}
  }).then(() => res.redirect('/users'))
}

module.exports = {
    signupPage,
    signUpUsers,
    loginPageUsers,
    loginUsers,
    updatePageUsers,
    updateUsers,
    deleteUsers
}