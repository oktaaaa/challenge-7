var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

const session = require('express-session')
const flash = require('express-flash')

const PORT = 8080;

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');

var app = express()


app.use(flash())
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

//public
app.use(express.static('public'));
//additional
app.use(session({
  secret: "secret",
  resave: false,
  saveUninitialized: false,
  //move to redis for persistent data
  //store: memoryStore
}))


app.use('/', indexRouter);
app.use('/users', usersRouter);



//port
app.listen(PORT, () => {
    console.log(`Server is listening on ${PORT}`);
})

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
