'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class createRooms extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  createRooms.init({
    name: DataTypes.STRING,
    player_1_id: DataTypes.INTEGER,
    player_2_id: DataTypes.INTEGER,
    player_1_hands: DataTypes.ARRAY,
    player_2_hands: DataTypes.ARRAY,
    results: DataTypes.ARRAY
  }, {
    sequelize,
    modelName: 'createRooms',
  });
  return createRooms;
};