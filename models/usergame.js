'use strict';
const {
  Model
} = require('sequelize');

const bcrypt = require('bcrypt')

module.exports = (sequelize, DataTypes) => {
  class UserGame extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      //disebutkan if not, it looks for UserGameId
      this.hasMany(models.UserGameHistory, {foreignKey: 'user_id', as: "UserGameBiodatum"})
      this.hasOne(models.UserGameBiodata, {foreignKey: 'user_id'})
    }

    static #hash = (password) => {
      return bcrypt.hashSync(password, 10)
    }

    static register = ({ username, password}) =>{
      console.log("register");
      const hashedPassword = this.#hash(password)
      
      return this.create({
        username,
        password: hashedPassword
      })
    }

    checkPassword = (password) =>{
      return bcrypt.compareSync(password, this.password)
    }

    generateToken = () => {
      const payload = {
        id: this.id,
        username: this.username
      }
      return jwt.sign(payload, "secret-key", {expiresIn: "15m"})
    }

    static authenticate = async ({ username, password }) =>{
      try{
        const user1 = await this.findOne({where: username})

        if(!user1){
          return Promise.reject('user not found')
        } 

        const isPasswordValid = user1.checkPassword(password)
        if(!isPasswordValid){
          return Promise.reject('wrong password')
        }
        return Promise.resolve(user1)
      }catch (error){
        return Promise.reject(error)
      }

      
    }

  }

  UserGame.init({
    username: DataTypes.STRING,
    password: DataTypes.ARRAY(DataTypes.STRING)
  }, {
    sequelize,
    modelName: 'UserGame',
  });
  return UserGame;
};