const passport = require('passport')
const {Strategy: JwStrategy, ExtractJwt} = require('passport-jwt');
const { UniqueConstraintError } = require('sequelize');
const {UserGame} = require('../models')

const options = {
    jwtFromRequest: ExtractJwt.fromHeader('authorization'),
    secretOrKey: 'secret-key',
}

passport.use(new JwStrategy(options, async(payload, done) =>{
    UserGame.findByPk(payload.id)
        .then(user => done(null, user))
        .catch(err => done(err, false))
}));
//callback ngambil data user

module.exports = passport